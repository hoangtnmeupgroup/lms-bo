import { BaseModel } from "./base.model";

export interface AccessToken extends BaseModel {
  id: string,
  ttl: number;
  created: Date;
  userId: number;
}

