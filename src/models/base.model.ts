export interface BaseModel {
  createdAt?: Date;
  updatedAt?: Date;
  isDeleted?: boolean;
}

