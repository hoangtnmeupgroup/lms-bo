import { BaseModel } from "./base.model";

export interface User extends BaseModel {
  id?: string;
  username?: string | null;
  fullname?: string | null;
  dateOfBirth?: string | null;
  phone?: string | null;
  email?: string | null;
  address?: string | null;
  city?: number | null;
  district?: number | null;
}
