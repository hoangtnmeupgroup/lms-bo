import { PayloadAction, createSlice } from "@reduxjs/toolkit";

import { RootState } from "../store";
import { User } from "models/user.model";
import { deleteToken } from "helpers/userToken";

interface AuthReducer {
  user?: User;
}
const initialState: AuthReducer = {
  user: undefined,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    updateUser: (state, action: PayloadAction<{ user: User }>) => {
      state.user = action.payload?.user;
    },
    userLogout: (state) => {
      state.user = undefined;
      deleteToken();
    },
  },
});

export const { updateUser, userLogout } = authSlice.actions;

export const currentUser = (state: RootState) => state.auth.user;

export default authSlice.reducer;
