import { PayloadAction, createSlice } from "@reduxjs/toolkit";

import { RootState } from "../store";
import { User } from "models/user.model";

interface UserReducer {
  users: User[];
}
const initialState: UserReducer = {
  users: [],
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    FetchUsers: (state, action) => {
      state = action.payload;
      return state;
    },
    AddUser: (state, action: PayloadAction<User>) => {
      state.users.push(action.payload);
      return state;
    },
    UpdateUser: (state, action: PayloadAction<User>) => {
      const index = state.users?.findIndex((el) => el.id === action.payload.id);
      const { ...obj } = action.payload;
      if (index && index > -1) {
        state.users[index] = {
          ...state.users[index],
          ...obj,
        };
        return state;
      }
    },
  },
});

export const { FetchUsers, AddUser, UpdateUser } = userSlice.actions;

export const listUser = (state: RootState) => state.user.users;

export default userSlice.reducer;
