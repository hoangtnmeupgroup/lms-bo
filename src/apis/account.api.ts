import { Observable, from, map } from "rxjs";
import { Method, request } from "../helpers/request.helper";
import { AccessToken } from "../models/accessToken.model";
import { AxiosResponse } from "axios";
import { User } from "models/user.model";

export class AccountAPI {
  static readonly COMPONENT_NAME: string = "Accounts";

  static login = ({
    email,
    password,
  }: {
    email: string;
    password: string;
  }): Observable<AccessToken> => {
    return from(
      request({
        method: Method.POST,
        url: `/${this.COMPONENT_NAME}/login`,
        data: {
          email,
          password,
        },
      })
    ).pipe(map((response: AxiosResponse<AccessToken>) => response.data));
  };

  static getAll = (): Observable<User[]> => {
    return from(
      request({
        method: Method.GET,
        url: `/${this.COMPONENT_NAME}`,
      })
    ).pipe(map((response: AxiosResponse<User[]>) => response.data));
  };

  static getMe = (): Observable<User> => {
    return from(
      request({
        method: Method.GET,
        url: `/${this.COMPONENT_NAME}/get-me`,
      })
    ).pipe(map((response: AxiosResponse<User>) => response.data));
  };

  static addUser = (user: User): Observable<User> => {
    return from(
      request({
        method: Method.POST,
        url: `/${this.COMPONENT_NAME}`,
        data: user,
      })
    ).pipe(map((response: AxiosResponse<User>) => response.data));
  };

  static updateUser = (user: User): Observable<User> => {
    return from(
      request({
        method: Method.PUT,
        url: `/${this.COMPONENT_NAME}`,
        data: user,
      })
    ).pipe(map((response: AxiosResponse<User>) => response.data));
  };
}
