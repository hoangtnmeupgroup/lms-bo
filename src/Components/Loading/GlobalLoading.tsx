import { Spin } from "antd";
import { useAppSelector } from "app/hooks";
import { selectLoading } from "app/reducers/loading.reducer";
import React from "react";

const GlobalLoading = () => {
  const loadingState = useAppSelector(selectLoading);
  const [loading, setLoading] = React.useState(loadingState);

  React.useEffect(() => {
    setLoading(loadingState);
  }, [loadingState]);

  return (
    <>
      {loading && (
        <div className="loading">
          <Spin size="large" />
        </div>
      )}
    </>
  );
};

export default GlobalLoading;
