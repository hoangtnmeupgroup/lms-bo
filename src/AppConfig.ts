/** @format */

export const AppConfig = {
  apiUrl: process.env.REACT_APP_API_ENDPOINT || "https://kosei.eupsolution.net/api",
  apiPancakeUrl: "https://pages.fm/api/v1",
  mapsAPIKey: process.env.GOOGLE_MAPS_API_KEY || "AIzaSyBVra377vUKJOAviq_OTOfU89NqmGOXhZA",
  routerBase: process.env.PUBLIC_URL || "",
};
