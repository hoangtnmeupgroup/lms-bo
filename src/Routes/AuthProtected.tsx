import { useMemo, useState } from "react";
import { useDispatch } from "react-redux";
import { Navigate } from "react-router-dom";


import { AccountAPI } from "apis/account.api";
import { updateUser, userLogout } from "app/reducers/auth.reducer";
import { doOnSubscribe, useRxEffect } from "helpers/rxjs.helper";
import { getToken } from "helpers/userToken";
import { User } from "models/user.model";
import { finalize, tap } from "rxjs";

const AuthProtected = (props: any) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const userToken = getToken();

  const getMe$ = useMemo(
    () =>
      AccountAPI.getMe().pipe(
        doOnSubscribe(() => setIsLoading(true)),
        tap({
          next: (user: User) => {
            dispatch(
              updateUser({
                user: user,
              })
            );
          },
          error: () => {
            dispatch(userLogout());
          },
        }),
        finalize(() => setIsLoading(false)) // hide loading on end
      ),
    [dispatch]
  );

  // get user info on start
  useRxEffect(() => getMe$, []);

  /*
    Navigate is un-auth access protected routes via url
    */

  // if (isLoading && !userToken) {
  //   return <Navigate to={{ pathname: "/login" }} />;
  // }

  return <>{props.children}</>;
};

export default AuthProtected;
