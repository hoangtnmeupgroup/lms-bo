import { useEffect } from "react";
import { Observable, defer, noop } from "rxjs";

export function useRxEffect(factory: () => Observable<unknown>, deps: unknown[]){
  useEffect(() => {
    const subscription = factory().subscribe({
      error: noop
    })
    return () => subscription.unsubscribe()
    // eslint-disable-next-line
  }, deps)
}
export function doOnSubscribe<T>(onSubscribe: () => void): (source: Observable<T>) => Observable<T> {
  return function inner(source: Observable<T>): Observable<T> {
    return defer(() => {
      onSubscribe();
      return source
    })
  }
}
