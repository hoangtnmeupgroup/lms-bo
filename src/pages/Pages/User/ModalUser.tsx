import { Form, Input, message, Select } from "antd";
import { User } from "models/user.model";
import { useEffect, useState } from "react";
import {
  Button,
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  Spinner,
} from "reactstrap";
import provinces from "../../../constants/provinces.json";
import Flatpickr from "react-flatpickr";
import { AccountAPI } from "apis/account.api";
import { doOnSubscribe } from "helpers/rxjs.helper";
import { finalize, noop, tap } from "rxjs";
const { Option } = Select;

interface ModalUserProps {
  isShowModal: boolean;
  handleShowModal: (el: boolean) => void;
  user?: User;
}

interface FormModal {
  id?: string | null;
  username?: string | null;
  fullname?: string | null;
  dateOfBirth?: string | null;
  phone?: string | null;
  email?: string | null;
  address?: string | null;
  city?: number | null;
  district?: number | null;
  password?: string | null;
  confirmPassword?: string | null;
}

interface District {
  name: string;
  code: number;
  division_type: string;
  codename: string;
  province_code: number;
  wards: unknown[];
}

const ModalUser = (props: ModalUserProps) => {
  const { isShowModal, handleShowModal, user } = props;

  const [form] = Form.useForm();
  const [formValue, setFormValue] = useState<FormModal>({
    id: null,
    username: null,
    fullname: null,
    dateOfBirth: null,
    email: null,
    address: null,
    city: null,
    district: null,
    password: null,
    confirmPassword: null,
  });

  const [listDistrict, setListDistrict] = useState<District[]>(
    provinces[0].districts
  );

  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    form.setFieldsValue({
      username: user?.username ? user.username : null,
      fullname: user?.fullname ? user.fullname : null,
      dateOfBirth: user?.dateOfBirth ? user.dateOfBirth : null,
      email: user?.email ? user.email : null,
      address: user?.address ? user.address : null,
      city: user?.city ? user.city : null,
      district: user?.district ? user.district : null,
      password: null,
      confirmPassword: null,
    });
    setFormValue({
      ...formValue,
      username: user?.username ? user.username : null,
      fullname: user?.fullname ? user.fullname : null,
      dateOfBirth: user?.dateOfBirth ? user.dateOfBirth : null,
      email: user?.email ? user.email : null,
      address: user?.address ? user.address : null,
      city: user?.city ? user.city : null,
      district: user?.district ? user.district : null,
      password: null,
      confirmPassword: null,
    });
  }, [user?.id]);

  const onValuesChange = (values: FormModal) => {
    setFormValue(values);
  };
  const onFinished = (values: FormModal) => {
    let userData: User = {
      username: values.username,
      fullname: values.fullname,
      dateOfBirth: values.dateOfBirth,
      phone: values.phone,
      email: values.email,
      address: values.address,
      city: values.city,
      district: values.district,
    };

    if (user?.id) {
      userData.id = user.id;
      // edit user
      AccountAPI.updateUser(userData)
        .pipe(
          doOnSubscribe(() => setLoading(true)), // show loading on start
          tap({
            next: (result: User) => {
              // show message toast
              message.success("Success").then();
            },
            error: () => message.error("Failed").then((r) => console.log(r)),
          }),
          finalize(() => setLoading(false))
        )
        .subscribe({ error: noop });
    } else {
      // add user
      AccountAPI.addUser(userData)
        .pipe(
          doOnSubscribe(() => setLoading(true)), // show loading on start
          tap({
            next: (result: User) => {
              // show message toast
              message.success("Success").then();
            },
            error: () => message.error("Failed").then((r) => console.log(r)),
          }),
          finalize(() => setLoading(false))
        )
        .subscribe({ error: noop });
    }
  };

  const onFinishFailed = () => {
    message
      .error("Merci de compléter toutes les colonnes.")
      .then((r) => console.log(r));
  };

  return (
    <>
      {/* Grids in Modals */}
      <Modal
        isOpen={isShowModal}
        toggle={() => {
          handleShowModal(!isShowModal);
        }}
      >
        <ModalHeader
          className="modal-title"
          toggle={() => {
            handleShowModal(!isShowModal);
          }}
        >
          {user ? "Edit User" : "Add User"}
        </ModalHeader>
        <ModalBody>
          <Form
            form={form}
            name={`${user?.id ? "editUser" : "addUser"}`}
            onFinish={onFinished}
            onValuesChange={onValuesChange}
            onFinishFailed={onFinishFailed}
            initialValues={formValue}
            colon={false}
            autoComplete="off"
          >
            <div className="row g-3">
              <Col xxl={6}>
                <div>
                  <label htmlFor="firstName" className="form-label">
                    Email
                  </label>
                  <Form.Item name="email" required>
                    <Input
                      type="text"
                      className="form-control"
                      placeholder="Enter email"
                    />
                  </Form.Item>
                </div>
              </Col>
              <Col xxl={6}>
                <div>
                  <label htmlFor="lastName" className="form-label">
                    User Name
                  </label>
                  <Form.Item name="userName" required>
                    <Input
                      type="text"
                      className="form-control"
                      placeholder="Enter user name"
                    />
                  </Form.Item>
                </div>
              </Col>

              <Col xxl={6}>
                <div>
                  <label htmlFor="password" className="form-label">
                    Password
                  </label>
                  <Form.Item name="password" required>
                    <Input.Password
                      style={{ display: "flex" }}
                      className="form-control"
                      placeholder="Enter password"
                    />
                  </Form.Item>
                </div>
              </Col>

              <Col xxl={6}>
                <div>
                  <label htmlFor="lastName" className="form-label">
                    Confirm Password
                  </label>
                  <Form.Item name="confirmPassword" required>
                    <Input.Password
                      style={{ display: "flex" }}
                      className="form-control"
                      placeholder="Enter password confirm"
                    />
                  </Form.Item>
                </div>
              </Col>

              <Col xxl={6}>
                <div>
                  <label htmlFor="role" className="form-label">
                    Type Role
                  </label>
                  <Form.Item name="role" required>
                    <Input
                      type="text"
                      className="form-control"
                      placeholder="Select role"
                    />
                  </Form.Item>
                </div>
              </Col>

              <Col xxl={6}>
                <div>
                  <label htmlFor="phone" className="form-label">
                    Phone
                  </label>
                  <Form.Item name="phone" required>
                    <Input
                      type="text"
                      className="form-control"
                      placeholder="Enter phone"
                    />
                  </Form.Item>
                </div>
              </Col>

              <Col xxl={6}>
                <div>
                  <label htmlFor="firstName" className="form-label">
                    City
                  </label>
                  <Form.Item name="city" required>
                    <Select
                      onChange={(el) => {
                        const tempDistrict = provinces.filter(
                          (province) => province.code === el
                        )[0].districts;
                        setListDistrict(tempDistrict);
                        form.setFieldValue("district", tempDistrict[0].code);
                      }}
                      placeholder="Select city"
                    >
                      {provinces.map((el) => (
                        <Option key={el.code} value={el.code}>
                          {el.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </div>
              </Col>

              <Col xxl={6}>
                <div>
                  <label htmlFor="district" className="form-label">
                    District
                  </label>
                  <Form.Item name="district" required>
                    <Select placeholder="Select district">
                      {listDistrict.map((el) => (
                        <Option key={el.code} value={el.code}>
                          {el.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </div>
              </Col>
              <Col xxl={6}>
                <div>
                  <label htmlFor="dateOfBirth" className="form-label">
                    Date of Birth
                  </label>
                  <Form.Item name="dateOfBirth">
                    <Flatpickr
                      className="form-control"
                      options={{
                        dateFormat: "d M, Y",
                      }}
                      placeholder="Select date"
                    />
                  </Form.Item>
                </div>
              </Col>

              <Col xxl={6}>
                <div>
                  <label htmlFor="address" className="form-label">
                    Address
                  </label>
                  <Form.Item name="address">
                    <Input
                      type="text"
                      className="form-control"
                      placeholder="Enter address"
                    />
                  </Form.Item>
                </div>
              </Col>

              <Col lg={12}>
                <div
                  className="hstack gap-2 justify-content-end"
                  style={{ alignItems: "center" }}
                >
                  <Button color="light" onClick={() => handleShowModal(false)}>
                    Close
                  </Button>
                  <Form.Item style={{ marginBottom: 0 }}>
                    <Button color="primary" htmltype="submit">
                      {loading && (
                        <Spinner size="sm" className="me-2">
                          {" "}
                          Loading...{" "}
                        </Spinner>
                      )}
                      Submit
                    </Button>
                  </Form.Item>
                </div>
              </Col>
            </div>
          </Form>
        </ModalBody>
      </Modal>
    </>
  );
};

export default ModalUser;
