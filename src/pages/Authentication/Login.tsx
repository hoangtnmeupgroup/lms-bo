import React, { useState } from "react";
import {
  Card,
  CardBody,
  Col,
  Container,
  Label,
  Row,
  Spinner,
} from "reactstrap";
import ParticlesAuth from "../AuthenticationInner/ParticlesAuth";

//redux

import { Link, useNavigate } from "react-router-dom";
import withRouter from "../../Components/Common/withRouter";
// Formik validation

// actions

import { Button, Checkbox, Form, Input, message } from "antd";
import { AccountAPI } from "apis/account.api";
import { doOnSubscribe } from "helpers/rxjs.helper";
import { setToken } from "helpers/userToken";
import { AccessToken } from "models/accessToken.model";
import { finalize, noop, tap } from "rxjs";
//import images

interface LoginData {
  email: string;
  password: string;
  remember: boolean;
}

const Login = () => {
  const navigate = useNavigate();
  const [formValue, setFormValue] = useState<LoginData>({
    email: "",
    password: "",
    remember: true,
  });
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const onFinish = (values: LoginData) => {
    setFormValue(values);
    
    // Call login API
    AccountAPI.login(values)
      .pipe(
        doOnSubscribe(() => setIsLoading(true)), // show loading on start,
        tap({
          next: (accessToken: AccessToken) => {
            // update user access token
            setToken(accessToken.id, values.remember);
            // show message toast
            message.success("Success").then();
            // navigate
            navigate("/dashboard");
          },
          error: () =>
            message.error("Login Failed").then((r) => console.log(r)),
        }),
        finalize(() => setIsLoading(false))
      )
      .subscribe({ error: noop });
  };

  const onFinishFailed = () => {
    message.error("Error").then((r) => console.log(r));
  };

  // Inside your component

  document.title = "SignIn | eUp Solution - Admin";
  return (
    <React.Fragment>
      <ParticlesAuth>
        <div className="auth-page-content mt-lg-5">
          <Container>
            <Row className="justify-content-center">
              <Col md={8} lg={6} xl={5}>
                <Card className="mt-4">
                  <CardBody className="p-4">
                    <div className="text-center mt-2">
                      <h5 className="text-primary">Welcome Back !</h5>
                      <p className="text-muted">
                        Sign in to continue to eUp Solution.
                      </p>
                    </div>

                    <div className="p-2 mt-4">
                      <Form
                        layout="vertical"
                        name="login"
                        initialValues={formValue}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                      >
                        <div className="mb-3">
                          <Label htmlFor="email" className="form-label">
                            Email
                          </Label>
                          <Form.Item name="email">
                            <Input
                              className="form-control"
                              placeholder="Enter email"
                              type="email"
                            />
                          </Form.Item>
                        </div>

                        <div className="mb-3">
                          <div className="float-end">
                            <Link to="/forgot-password" className="text-muted">
                              Forgot password?
                            </Link>
                          </div>
                          <Label
                            className="form-label"
                            htmlFor="password-input"
                          >
                            Password
                          </Label>
                          <div className="position-relative auth-pass-inputgroup mb-3">
                            <Form.Item name="password">
                              <Input.Password
                                size="large"
                                className="pe-5"
                                placeholder="Enter Password"
                              />
                            </Form.Item>
                          </div>
                        </div>

                        <div className="form-check">
                          <Form.Item name="remember" valuePropName="checked">
                            <Checkbox>
                              <Label
                                className="form-check-label"
                                htmlFor="auth-remember-check"
                              >
                                Remember me
                              </Label>{" "}
                            </Checkbox>
                          </Form.Item>
                        </div>

                        <div className="mt-4">
                          <Form.Item>
                            <Button
                              type="primary"
                              htmlType="submit"
                              className="btn btn-success w-100"
                              style={{ padding: 5 }}
                            >
                              {isLoading && (
                                <Spinner size="sm" className="me-2">
                                  {" "}
                                  Loading...{" "}
                                </Spinner>
                              )}
                              Sign In
                            </Button>
                          </Form.Item>
                        </div>
                      </Form>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
      </ParticlesAuth>
    </React.Fragment>
  );
};

export default withRouter(Login);
